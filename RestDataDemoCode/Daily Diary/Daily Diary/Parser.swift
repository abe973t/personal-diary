//
//  Parser.swift
//  Daily Diary
//
//  Created by Abraham Tesfamariam on 7/13/17.
//  Copyright © 2017 RJT Compuquest. All rights reserved.
//

import Foundation
import UIKit

enum ReadError: Error {
    case fileReadError
    case conversionError
}

struct Thought {
    var title, thoughtStr, imgName: String?
    var img: UIImage?
}

struct Parser {
    func readPlist() throws -> [Thought] {
        let docsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let imgsDirURL = docsURL.appendingPathComponent("Images")
        var imgURL: URL?
        var thoughts: [Thought] = []
        do {
            // Get the directory contents urls
            let docsContents = try FileManager.default.contentsOfDirectory(at: docsURL, includingPropertiesForKeys: nil, options: [])
            
            // filter directory contents for tableView
            let plistFiles = docsContents.filter{ $0.pathExtension == "plist" }
            
            for plists in plistFiles {
                // converts url to NSDictionary object
                guard let plist = NSDictionary(contentsOf: plists) as? [String:String] else {
                    throw ReadError.conversionError
                }
                
                // extracts properties from plist file
                if let title = plist["title"], let thought = plist["thoughts"], let imgName = plist["imageName"] {
                    // creates image files and a Thought objects, appends thoughts array
                    imgURL = imgsDirURL.appendingPathComponent(imgName)
                    let img = UIImage(contentsOfFile: (imgURL?.absoluteString)!)
                    thoughts.append(Thought(title: title, thoughtStr: thought, imgName: imgName, img: img))
                }
            }
            
            /* Used for getting plist File names
            plistFileNames = plistFiles.map{ $0.deletingPathExtension().lastPathComponent }
            print("🔴 plist list:", plistFileNames)
            */
            
        } catch {
            print(error.localizedDescription)
        }
        return thoughts
    }
}
