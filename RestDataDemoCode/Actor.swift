//
//  Actor.swift
//  RestDataDemoCode
//
//  Created by Balu Naik on 7/6/17.
//  Copyright © 2017 RJTCOMPUQUEST. All rights reserved.
//

import UIKit

class Actor: NSObject {
    
    var name:String?
    var descripton:String?
    var dob:String?
    var country:String?
    var height:String?
    var spouse:String?
    var children:String?
    var imageUrl:String?



    init(name:String,descripton:String,dob:String,country:String,height:String,spouse:String,children:String,imageUrl:String) {
        
        self.name = name
        self.descripton = descripton
        self.dob = dob
        self.country = country
        self.height = height
        self.spouse = spouse
        self.children = children
        self.imageUrl = imageUrl
        
    }

}

class Email: NSObject {
    var userID: Int?
    var id: Int?
    var title: String?
    var body: String?
    
    init(uID: Int, id: Int, title: String, body: String) {
        self.userID = uID
        self.body = body
        self.title = title
        self.id = id
    }
}
