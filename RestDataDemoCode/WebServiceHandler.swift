//
//  WebServiceHandler.swift
//  RestDataDemoCode
//
//  Created by Balu Naik on 7/6/17.
//  Copyright © 2017 RJTCOMPUQUEST. All rights reserved.
//

import UIKit

let urlString = "http://microblogging.wingnity.com/JSONParsingTutorial/jsonActors"
let urlString2 = "http://jsonplaceholder.typicode.com/posts/"

typealias completionHandler = (Any) -> ()

/*extension UIImageView {
    public func imageFromUrl(urlString: String) {
        if let url = URL(string: urlString) {
            let request = URLRequest(url: url as URL)
            NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {
                (response: URLResponse?, data: Data?, error: Error?) -> Void in
                if let imageData = data as NSData? {
                    self.image = UIImage(data: imageData as Data)
                }
            }
        }
    }
}*/

extension UIImageView {
    public func imageFromUrl(urlString: String) {
        let urlData = URL(string: urlString)
        
        DispatchQueue.global().async {
            let data = try? Data(contentsOf: urlData!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
            DispatchQueue.main.async {
                self.image = UIImage(data: data!)
            }
        }
        
        /*if let url = URL(string: urlString) {
            URLSession.shared.dataTask(with: url) { (data, response, error) in
                
                if error == nil {
                    self.image = UIImage(data: data as! Data)
                }

            }
        
        }*/
    }
}


class WebServiceHandler: NSObject {
    static let sharedInstance = WebServiceHandler()
    
    func getActorArray(completion:@escaping completionHandler) {
        let urlRequest = URL(string: urlString)
        
        URLSession.shared.dataTask(with: urlRequest!) { (data, response, error) in
            
            if error == nil {
                do {
                    if let jsonResult =  try JSONSerialization.jsonObject(with: data!, options:[]) as? Dictionary<String, Any>{
                        
                        guard let arr = jsonResult["actors"] as? [[String:Any]] else{
                            return
                        }
                        var arrayActor:Array<Actor> = []
                        for item in arr{
                            
                            let actor = Actor(name: item["name"] as! String, descripton: item["description"] as! String, dob: item["dob"] as! String, country: item["country"] as! String, height: item["height"] as! String, spouse: item["spouse"] as! String, children: item["children"] as! String, imageUrl: item["image"] as! String)
                            arrayActor.append(actor)
                        }
                        completion(arrayActor)
                    }
                } catch {
                    print(error)
                }
                
            }
            
        }.resume()
        
    }
    
    func getDiffArray() -> [Email] {
        
        let urlRequest = URL(string: urlString2)
        var email: Email?
        var emailArr: [Email] = []
        
        URLSession.shared.dataTask(with: urlRequest!) { (data, response, error) in
            
            if error == nil {
                do {
                    if let jsonResult =  try JSONSerialization.jsonObject(with: data!, options:[]) as? [[String:Any]] {
                        
                        
                        
                        for item in jsonResult {
                            
                            email = Email(uID: item["userId"] as! Int, id: item["id"] as! Int, title: item["title"] as! String, body: item["body"] as! String)
                            
                            emailArr.append(email!)
                        }
                        
                        
                        print(email!.body!)
                    }
                } catch {
                    print(error)
                }
                
            }
            
        }.resume()
        
        return emailArr
    }

}
