//
//  EntriesVC.swift
//  Daily Diary
//
//  Created by Abraham Tesfamariam on 7/10/17.
//  Copyright © 2017 RJT Compuquest. All rights reserved.
//

import UIKit

class EntriesVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var srchBar: UISearchBar!
    
    // two arrays because on of them will be filtered when user searches and we need an original list to refer back to
    var entries: [Thought] = []
    var originalEntries: [Thought] = []
    var reader = Parser()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // assign wallpaper to tableView
        let wallpaper = UIImageView(image: #imageLiteral(resourceName: "ddWallpaper"))
        tblView.backgroundView = wallpaper
        originalEntries = entries
        
        do {
            try entries = reader.readPlist()
        } catch {
            print(error.localizedDescription)
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return entries.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        
        cell?.textLabel?.text = entries[indexPath.row].title
        
        return cell!
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        if let str = searchBar.text {
            let predicate = NSPredicate(format: "%K CONTAINS[c] %@", str)
            let tempArray = NSArray(array: entries)
            entries = NSArray(array: tempArray.filtered(using: predicate)) as! [Thought]
            DispatchQueue.main.async {
                self.tblView.reloadData()
            }
        }
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        if (searchBar.text?.isEmpty)! {
            entries = originalEntries
            DispatchQueue.main.async {
                self.tblView.reloadData()
            }
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if entries.count == 0 {
            let alrt = UIAlertController(title: "Alert", message: "Your search produced no results!", preferredStyle: .alert)
            let ok = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alrt.addAction(ok)
            self.present(alrt, animated: true, completion: nil)
        }
    }

    /*
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
