//
//  ThoughtsVC.swift
//  Daily Diary
//
//  Created by Abraham Tesfamariam on 7/11/17.
//  Copyright © 2017 RJT Compuquest. All rights reserved.
//

import UIKit

class ThoughtsVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextViewDelegate {
    
    /*
     The user will create have the option to enter a title in a text field, enter his/her thoughts in a textview and add an image to the entry.
     
     Each Diary Item should be saved in it's own PLIST file. The name of the PLIST file will <DIARY_TITLE>.plist. The plist file should have fields to represent the thoughts of the user and the name of the image file associated with that entry. The images should be saved in a Images folder that you will create within the Documents folder.
     
     In a table view, display a list of titles. When the user clicks on a title, open up the corresponding PLIST file and display it's contents in a detail view controller. If the user wants, he should be able to edit his/her thoughts only. The title and image cannot be edited/modified.
     */
    
    /*
     TODO:
        make it so that when user skips a field when creating an entry it doesn't crash
        erase directory and reset simulator and try to save an entry, doesn't save entries for Mohit
        use present and dismiss instead of segues
        make image name textfield a lighter color
    */
    
    var imgPicker = UIImagePickerController()
    @IBOutlet weak var txtView: UITextView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var titleTxt: UITextField!
    @IBOutlet weak var imgNameTextField: UITextField!
    let placeholder = "What's on your mind? Begin typing your thoughts here. 😁"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        txtView.textColor = UIColor.lightGray
    }
    
    // replaces placeholder with user input
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == placeholder {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    
    // fills textView with placeholder if user deletes the text
    func textViewDidEndEditing(_ textView: UITextView) {
        if txtView.text.isEmpty {
            textView.textColor = UIColor.lightGray
            txtView.text = placeholder
        }
    }
    
    @IBAction func openGalleryAction(_ sender: UIButton) {
        
        imgPicker.sourceType = .photoLibrary
        imgPicker.allowsEditing = true
        imgPicker.delegate = self
        present(imgPicker, animated: true, completion: nil)
        
    }
    
    @IBAction func openCamera(_ sender: UIButton) {
        // checks if camera is available first, presents alert if not available
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imgPicker.sourceType = .camera
            imgPicker.delegate = self
            present(imgPicker, animated: true, completion: nil)
        } else {
            let alrt = UIAlertController(title: "Caution", message: "Camera not available.", preferredStyle: .alert)
            let ok = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alrt.addAction(ok)
            present(alrt, animated: true, completion: nil)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        imgView.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func sbmtBttnClicked(_ sender: UIButton) {
        // checks if required fields are filled
        var message: String?
        let alrt = UIAlertController(title: "Caution", message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alrt.addAction(ok)
        
        if(titleTxt.text?.isEmpty)! {
            message = "Diary must have a title."
            present(alrt, animated: true, completion: nil)
        } else if (txtView.text.isEmpty || txtView.text == placeholder) {
            message = "Must have some thoughts written down!"
            present(alrt, animated: true, completion: nil)
        } else if ((imgNameTextField.text?.isEmpty)! && imgView.image != nil) {
            message = "Must have a title for image."
            present(alrt, animated: true, completion: nil)
        }
        
        // path for Documents folder in the sandbox
        let docsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        
        // creates new plist file in Documents
        let newPlistPath = docsURL.appendingPathComponent("\(titleTxt.text!).plist")
        let data: [String:String] = ["title":titleTxt.text!, "thoughts": txtView.text, "imageName":imgNameTextField.text!]
        NSDictionary(dictionary: data).write(to: newPlistPath, atomically: true)
        
        do {
            // creates Images directory in Documents
            let imgDir = docsURL.appendingPathComponent("Images")
            try FileManager.default.createDirectory(at: imgDir, withIntermediateDirectories: true, attributes: nil)
            
            // adds image file from imageView to the Images directory
            let imgFile = imgDir.appendingPathComponent(imgNameTextField.text!)
            if let image = imgView.image {
                FileManager.default.createFile(atPath: imgFile.absoluteString, contents: UIImagePNGRepresentation(image), attributes: nil)
            }
        } catch {
            print(error.localizedDescription)
        }
        print(docsURL.absoluteString)
        present(EntriesVC(), animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // appends entries array with a Thought object when save button is clicked
        if segue.identifier == "save" {
            if let dVC = segue.destination as? EntriesVC {
                dVC.entries.append(Thought(title: titleTxt.text!, thoughtStr: txtView.text!, imgName: imgNameTextField.text!, img: imgView.image!))
            }
        }
    }
    
    @IBAction func cancelBtnClicked(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}
