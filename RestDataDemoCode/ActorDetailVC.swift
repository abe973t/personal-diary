//
//  SecondViewController.swift
//  RestDataDemoCode
//
//  Created by Balu Naik on 7/6/17.
//  Copyright © 2017 RJTCOMPUQUEST. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController,UITableViewDataSource {

    @IBOutlet weak var tblview: UITableView!
    var actorArray: Array<Actor> = []
    var emailArray: [Email] = []
    var refresh = UIRefreshControl()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        refresh.isEnabled = true
        refresh.tintColor = UIColor.black
        refresh.addTarget(self, action: #selector(ActorListViewController.refreshAction(_:)), for: .valueChanged)
        
        tblview.addSubview(refresh)
        tblview.tableFooterView = UIView()
        tblview.rowHeight = UITableViewAutomaticDimension   // dynamically adjusts row height
        tblview.estimatedRowHeight  = 70
        
        getApiCallForActors()
        getAPICallForEmails()
        
        print(emailArray)
    }
    
    func refreshAction(_ sender:Any)  {
        actorArray.removeAll()
        getApiCallForActors()
        print("hello")
    }
    
    func getApiCallForActors() {
        
        WebServiceHandler.sharedInstance.getActorArray { (actorArra) in
            if let arr = actorArra as? [Actor] {
                self.actorArray = arr
                DispatchQueue.main.async {
                    self.refresh.endRefreshing()
                    self.tblview.reloadData()
                }
            }
        }
    }
    
    func getAPICallForEmails() {
        self.emailArray = WebServiceHandler.sharedInstance.getDiffArray()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return actorArray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Basic", for: indexPath) as! ActorTableViewCell
        let  actor = actorArray[indexPath.row]
        
        cell.lblName?.text = actor.name
        cell.lblDesc?.text = actor.descripton
        cell.imageView?.imageFromUrl(urlString: actor.imageUrl!)
        
        return cell
    }

}
