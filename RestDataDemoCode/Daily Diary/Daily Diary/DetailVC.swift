//
//  DetailVC.swift
//  Daily Diary
//
//  Created by Abraham Tesfamariam on 7/13/17.
//  Copyright © 2017 RJT Compuquest. All rights reserved.
//

import UIKit

class DetailVC: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var thoughtsTxtView: UITextView!
    @IBOutlet weak var imgView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cancelBtnClicked(_ sender: UIButton) {
        // add an alert to ask user if theyre sure if they wanna discard their changes (if they made any)
        if let controller = storyboard?.instantiateViewController(withIdentifier: "entries") {
            
        }
        dismiss(animated: true, completion: nil)
    }
    
    

}
