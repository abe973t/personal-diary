//
//  ActorListViewController.swift
//  RestDataDemoCode
//
//  Created by Balu Naik on 7/6/17.
//  Copyright © 2017 RJTCOMPUQUEST. All rights reserved.
//

import UIKit

class ActorListViewController: UIViewController,UITableViewDataSource {

    /* NOTES:
        error status 200 means everything is ok
        line 65: sync will wait till the task is finished, async will run the next line regardless
    */
    
    /* QUESTIONS:
        line 75: why do we use resume? what the default state?
        line 24: why couldn't we use [[String:Any]]?
        why do we need to make an IBOutlet for a tableView object? It already has its own
        what is the meaning of sharedInstance?
    */
    
    @IBOutlet weak var tblview: UITableView!
    var actorArray: Array<[String:Any]> = []
    var refresh = UIRefreshControl()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblview.tableFooterView = UIView()
        refresh.isEnabled = true
        refresh.tintColor = UIColor.red
        refresh.addTarget(self, action: #selector(ActorListViewController.refreshAction(_:)), for: .valueChanged)
        refresh.attributedTitle = NSAttributedString(string: "Reloading!!")
        self.tblview.addSubview(refresh)
        getApiCallForActors()
    }
    
    func refreshAction(_ sender:Any) {
        actorArray.removeAll()
        getApiCallForActors()
        print("hello")
    }

    func getApiCallForActors() {
        
        let urlRequest = URL(string: urlString)
        
        URLSession.shared.dataTask(with: urlRequest!) { (data, response, error) in
            
            if error == nil {
                
                do {
                    if let jsonResult =  try JSONSerialization.jsonObject(with: data!, options:[]) as? Dictionary<String, Any> {
                        
                        guard let arr = jsonResult["actors"] as? [[String:Any]] else {
                           return
                        }
                        
                        self.actorArray = arr
                        
                        // used for main thread, always put stuff in the main thread
                        /*
                         sync means the function WILL BLOCK the current thread until it has completed, async means it will be handled in the background and the function WILL NOT BLOCK the current thread.
                        */
                        DispatchQueue.main.sync {
                            self.refresh.endRefreshing()
                            self.tblview.reloadData()
                        }
                    }
                    
                } catch {
                    print(error)
                }
                
            }
            
        }.resume()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return actorArray.count
    }

    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Basic", for: indexPath)
        let dict = actorArray[indexPath.row] as Dictionary
        
        cell.textLabel?.text = dict["name"] as! String?

        return cell
    }
    



}
